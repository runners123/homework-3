/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ramazan
 */
public class LoginServlet extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String username = request.getParameter("username");
        String pass = request.getParameter("password");
        String error = "";
        HttpSession session = request.getSession();
        Cookie[] cookies = request.getCookies();
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        /////Login with web.xml params/////
        String paramLogin = this.getServletConfig().getInitParameter("Username");
        String paramPass = this.getServletConfig().getInitParameter("Password");
        System.out.println(paramLogin + " web " + paramPass);
        System.out.println(username + " username " + pass);
        
        if(request.getParameter("registr")!=null){
            response.sendRedirect("RegistrServlet");
        }
        
        if(username != null && pass !=null && !username.equals("") && !pass.equals("") && cookies.length >1 ) {
            if(username.equals(paramLogin) && pass.equals(paramPass)){
                 session.setAttribute("verified", "yes");
                 session.setMaxInactiveInterval(60*60);
                request.setAttribute("Username", username);
                rd.forward(request, response);
            } else
                if(username.equals(cookies[2].getValue()) && pass.equals(cookies[3].getValue())){
                    session.setAttribute("verified", "yes");
                    session.setAttribute("username", username);
                    session.setMaxInactiveInterval(60*60);
                    request.setAttribute("Username", username);
                    rd.forward(request, response);
                } else {
                    error = "Username or password is wrong";
                }
        }
        
        PrintWriter out;
        try {
            out = response.getWriter();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Login</title>");
            out.println("</head>");
            out.println("<body style='background-color:#c4c4c4'>");
            out.println("<h2 style='text-align: center'>Log In</h2>");
            out.println("<form action='' method='post' style='text-align:center; padding-top:100px'>");
            
            out.println("<h3 style='color:red'>"+error+"</h3></br>");
            
            out.println("Username:</br>");
            out.println("<input type='text' name='username'/> </br>");
            out.println("Password:</br>");
            out.println("<input type='password' name='password'/> </br>");
            out.println("<input type='submit' value='Log In'/>");
            out.println("<form>");
            out.println("<input type='submit' name='registr' value='Registr'>");
            out.println("</body>");
            out.println("</html>");
            
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
