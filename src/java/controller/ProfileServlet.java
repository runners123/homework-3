/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ramazan
 */
public class ProfileServlet extends HttpServlet { 
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
            Cookie[] cookies = request.getCookies();
            String username = (String) request.getParameter("username");
            String pass = (String) request.getParameter("password");
            String name = (String) request.getParameter("name");
            String email = (String) request.getParameter("email");
            String bdate = (String) request.getParameter("bdate");   
            String act = (String) request.getParameter("action");   
            String error = "";
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Register</title>");
            out.println("</head>");
            out.println("<body style='background-color:#c4c4c4'>");
               
            if(act==null){        
            out.println("<form action='' method='post' style='text-align:center; padding-top:100px'>");
            out.println("Full Name:</br>");
            out.println("<input type='text' value='"+cookies[1].getValue()+"' name='name'/> </br>");
            out.println("Username:</br>");
            out.println("<input type='text' value='"+cookies[2].getValue()+"'  name='username'/> </br>");
            out.println("Password:</br>");
            out.println("<input type='text' value='"+cookies[3].getValue()+"' name='password'/> </br>");
            out.println("Email:</br>");
            out.println("<input type='text' value='"+cookies[4].getValue()+"' name='email'/> </br>");
            out.println("Birtdate:</br>");
            out.println("<input type='date' value='"+cookies[5].getValue()+"'  name='bdate'/> </br>");
            out.println("<input type='submit' name='action' value='Update'/>");
            out.println("<form>");
            
            }else{
                 if(username==null || username.equals("") || pass ==null || pass.equals("") ||
                 name==null || name.equals("") || email ==null || email.equals("") || 
                 bdate==null || bdate.equals("")){error+= "Please fill all the fields<br/>";}
              
                 if(!isValidEmailAddress(email)){error+= "Please enter valid email address<br/>";}  
      
           
                if(!error.equals("")){

                 out.println(error);
                }else{
                
                Cookie cname = new Cookie("name",name);
                cname.setMaxAge(60*60*24); 
                response.addCookie( cname );

                Cookie cusername = new Cookie("username",username);
                cusername.setMaxAge(60*60*24); 
                response.addCookie( cusername );

                Cookie cpassword = new Cookie("password",pass);
                cpassword.setMaxAge(60*60*24); 
                response.addCookie( cpassword );

                Cookie cemail = new Cookie("email",email);
                cemail.setMaxAge(60*60*24); 
                response.addCookie( cemail );          

                Cookie cbdate = new Cookie("bdate",bdate);
                cbdate.setMaxAge(60*60*24); 
                response.addCookie( cbdate );      
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
                request.setAttribute("Username", username);
                 session.setAttribute("username", username);
                requestDispatcher.forward(request, response);
            }    
            }  
          
         
            
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    public boolean isValidEmailAddress(String email) {
           String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
           java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
           java.util.regex.Matcher m = p.matcher(email);
           return m.matches();
    }// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
