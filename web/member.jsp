 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
   <%@include  file = "header.jsp" %>

	<div class='title'><h2>Members</h2></div>

	<div class='wrapper'>
		<div class='content'>

			<ul class='member'>

				<li>
					<a href='ramil.jsp'><img src='members/ramil.jpg'>
					<div class='caption'>Ramil Guluzade</div></a>
				</li>

				<li>
					<a href='bulud.jsp'><img src='members/bulud.jpg'>
					<div class='caption'>Buludkhan Alizade</div></a>
				</li>

				<li>
					<a href='ramazan.jsp'><img src='members/ramazan.jpg'>
					<div class='caption'>Ramazan Garaisayev</div></a>
				</li>

				<li>
					<a href='murad.jsp'><img src='members/murad.jpg'>
					<div class='caption'>Murad Huseynli</div></a>
				</li>

				<li>
					<a href='safura.jsp'><img src='members/safura.jpg'>
					<div class='caption'>Safura Jafarova</div></a>
				</li>

				<li>
					<a href='ilkin.jsp'><img src='members/ilkin.jpg'>
					<div class='caption'>Ilkin Huseynov</div></a>
				</li>
			</ul>	

			<div style='float:none;clear:both;'></div>
		</div>	
	</div>	

	<footer>
		<p>
			All Rights Reserved</br>
			©  The Runners 2015-2016
		</p>
	</footer>


</body>
</html>
