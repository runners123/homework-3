<%-- 
    Document   : header
    Created on : Nov 12, 2015, 7:46:59 PM
    Author     : Ramazan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% 
    if(session != null){
        String verified = (String)session.getAttribute("verified");
        if(verified!=null && verified.equals("yes")){
            //ok
        } else {
            response.sendRedirect("LoginServlet");
        }
    }%>
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Ilkin Huseynov, Ramazan Garaisayev" />
	<meta name="keywords" content="members, members of project, members of runners" />
	<meta name="description" content="This is the members page of our website." />
	<title>The Runners</title>
	<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
</head>
 


<body>
<!--HEADER START-->	
	<header>
	
		<div class='head'>
			<div class='wrapper'>
				<img  src='images/logo.png' alt='Runners'/>
			
				<ul>
					<li><a href='index.jsp'>Home</a></li>
					<li><a href='member.jsp'>Members</a></li>
					<li><a href='contact.jsp'>Contact us</a></li>
				</ul>	

			</div>
		</div>


		<div class='wrapper'>
			<div class='quote'>
				<p> 
					”Try not to become a person of success,
					but rather try to become a person of value.”
				<p/>
				<p class="author"><b>  ~ Albert Einstein </b> </p> 
			</div>
		</div>
	
	</header>		
<!--HEADER END-->
